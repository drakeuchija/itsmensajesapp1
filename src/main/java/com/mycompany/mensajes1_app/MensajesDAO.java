package com.mycompany.mensajes1_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class MensajesDAO {
     public static void CrearMensajeDB(Mensajes mensaje){
        Conexion db_connect = new  Conexion();
        try(Connection conexion=db_connect.get_Connection()){
            PreparedStatement ps = null;
            try{
                String query ="INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?,?)";
                ps=conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.autor_mensaje);
                ps.executeUpdate();
                System.out.println("Mensaje Creado");
                ps.close();
            }catch (SQLException e){
                System.out.println(e);
            }  
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
     public static void EditarMensajeDB(Mensajes mensaje){
     Conexion db_connect = new  Conexion();
        try(Connection conexion=db_connect.get_Connection()){
            PreparedStatement ps = null;
            ResultSet rs = null;
            try{
                String sql ="UPDATE mensajes SET mensaje=?, autor_mensaje=? WHERE id_mensaje=?";
                ps=conexion.prepareStatement(sql);
                String nwmsn , nwautor;
                System.out.println("Escribir Nuevo Mensaje");
                Scanner sc = new Scanner(System.in);
                nwmsn=sc.nextLine();
                System.out.println("Escribir Nuevo Autor");
                nwautor=sc.nextLine();
                ps.setString(1,nwmsn);
                ps.setString(2, nwautor);
                ps.setInt(3, mensaje.id_mensaje);
                ps.executeUpdate();
                System.out.println("Mensaje Actualizado");
                ps.close();
            }catch (SQLException e){
                System.out.println(e);
            }  
        } catch (Exception ex) {
            System.out.println(ex);
        }
       
     }
     public static void BorrarMensajeDB(Mensajes mensaje){
     Conexion db_connect = new  Conexion();
        try(Connection conexion=db_connect.get_Connection()){
            PreparedStatement ps = null;
            try{
                //DELETE FROM `mensajes` WHERE 0
                String query ="DELETE FROM mensajes WHERE id_mensaje=?";
                ps=conexion.prepareStatement(query);
                ps.setInt(1,mensaje.id_mensaje);
                ps.execute();
                System.out.println("Mensaje Eliminado");
                ps.close();
            }catch (SQLException e){
                System.out.println(e);
            }  
        } catch (Exception ex) {
            System.out.println(ex);
        }
     }
     public boolean BuscarMensajeDB(Mensajes mensaje){
     Conexion db_connect = new  Conexion();
     boolean existe=false;
        try(Connection conexion=db_connect.get_Connection()){
            PreparedStatement ps = null;
            try{
                String query ="SELECT * FROM mensajes WHERE id_mensaje=?";
                ps=conexion.prepareStatement(query);
                ps.setInt(1, mensaje.id_mensaje);
                ResultSet rs=ps.executeQuery();
                while(rs.next()){
                    System.out.println("id:"+rs.getInt("id_mensaje"));
                    System.out.println("mensaje:"+rs.getString("mensaje"));
                    System.out.println("Autor:"+rs.getString("autor_mensaje"));
                    System.out.println("Fecha:"+rs.getString("fecha_mensaje"));
                }
             System.out.println("Mensaje encontrado");
             existe=true;
             ps.close();
            }catch (SQLException e){
                System.out.println(e);
            }  
        } catch (Exception ex) {
            System.out.println(ex);
        }
         return existe;
    }
    
}
