package com.mycompany.mensajes1_app;

import java.sql.Connection;
import java.util.Scanner;

public class Inicio {
    public static void main(String [] args ){
        Conexion conexion=new Conexion();
        try (Connection cnx=conexion.get_Connection()){
            
        } catch (Exception e) {
            System.out.print(e);
        }

    Scanner sc=new Scanner(System.in);
    int opcion=0;
    do{
    System.out.println("*************************");
    System.out.println("Aplicacion de Mensajes");
    System.out.println("1.- Crear Mensaje");
    System.out.println("2.- Buscar Mensaje");
    System.out.println("3.- Editar Mensaje");
    System.out.println("4.- Eliminar Mensaje");
    System.out.println("5.- Salir");
    
        opcion=sc.nextInt();
        switch(opcion){
            case 1:
                MensajeService.crearMensaje();
            break;
            case 2:
                MensajeService.BuscarMensaje();
            break;
            case 3:
                MensajeService.editarMensaje();
            break;
            case 4:
                MensajeService.borrarMensaje();
            break;
            default:
            break;
        }
       }while (opcion!=5);
    }
}
