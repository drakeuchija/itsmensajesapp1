package com.mycompany.mensajes1_app;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {
    public Connection get_Connection(){
     Connection conection=null;
        try {
            conection=DriverManager.getConnection("jdbc:mysql://localhost:3306/mensajes_app","root","");
            if(conection!=null){
                System.out.println("Conexion Existosa");
            }
        } catch (Exception e) {
            System.err.println(e);
            System.err.println("Conexion Fallida");
        }
        return conection;
    }
}
