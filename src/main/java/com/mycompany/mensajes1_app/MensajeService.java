package com.mycompany.mensajes1_app;

import java.util.Scanner;

public class MensajeService {
    public static void crearMensaje(){
       Scanner sc = new Scanner(System.in); 
       System.out.println("Escribir Mensaje");
       String mensaje=sc.nextLine();
       System.out.println("Autor del Mensaje");
       String nombre=sc.nextLine();
       
       Mensajes registro = new Mensajes();
       registro.setMensaje(mensaje);
       registro.setAutor_mensaje(nombre);
       
       MensajesDAO.CrearMensajeDB(registro);
    }
    public static void BuscarMensaje(){
     Scanner sc = new Scanner(System.in); 
       System.out.println(" Escribir Id del Mensaje que desea Encontrar");
       int id_mensaje=sc.nextInt();
       Mensajes reg = new Mensajes();
       reg.setId_mensaje(id_mensaje);
       MensajesDAO op=new MensajesDAO();
       op.BuscarMensajeDB(reg);
       Mensajes registro = new Mensajes();
       registro.setId_mensaje(id_mensaje);
    }
    public static void editarMensaje(){
       
       Scanner sc = new Scanner(System.in); 
       System.out.println("Escribir Id del Mensaje que desea Editar");
       int id_mensaje=sc.nextInt();
       Mensajes reg = new Mensajes();
       reg.setId_mensaje(id_mensaje);
       MensajesDAO op=new MensajesDAO();
       
       if(op.BuscarMensajeDB(reg)){
       Mensajes registro = new Mensajes();
       registro.setId_mensaje(id_mensaje);
       MensajesDAO.EditarMensajeDB(registro);
       }
    }
    
    public static void borrarMensaje(){
    Scanner sc = new Scanner(System.in);
    System.out.println("Escribir Id del Mensaje que desea Borrar");
    int id_mensaje=sc.nextInt();
    Mensajes reg = new Mensajes();
    reg.setId_mensaje(id_mensaje);
    MensajesDAO op=new MensajesDAO();
    op.BuscarMensajeDB(reg);
    System.out.println("Esta seguro de borrar este mensaje");
    System.out.println("1 Yes o cualquier otro numero para No");
    int yes=sc.nextInt();
    if(yes==1){
         MensajesDAO.BorrarMensajeDB(reg);
    }else{
    System.out.println("Mensaje No Eliminado");    
      }
   }
}
